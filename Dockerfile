FROM nginxinc/nginx-unprivileged:stable-alpine
WORKDIR /usr/share/nginx/html
COPY docker/nginx.conf /etc/nginx/conf.d/default.conf
COPY public/build/ .
EXPOSE 8080/tcp
ENTRYPOINT ["nginx", "-g", "daemon off;"]
